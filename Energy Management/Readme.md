# Energy Management

## Quesstion 1 : What are the activities you do that make you relax - Calm quadrant?

* Meditation for few minutes help me calm my mind and my body relaxed.

* I take a walk every evening to get my body warmed up and to clear my mind of the many thoughts that go through it.

* Good conversations with family and friends help me reduce my stress and focus on things that are more important.

* Watching movies helps me redirect my mind from my daily routine and relax for a while. 



## Question 2 : When do you find getting into the Stress quadrant?

* I am anxious. if I have stuff to do but don't have a plan for how to complete it.

* I become upset if I can't be there for my friends and family when they need me.





## Question 3 : How do you understand if you are in the Excitement quadrant?

* I get really excited to go on outings with my friends.

* I'm glad when I finish my task ahead of schedule and have time to work on other things.

* Food that tastes excellent makes me happy.

* I get excited when I reach the goals I've set for my life.

* Travelling to new places makes me feel excited.

* I feel fulfilled when I work on creative projects and learn new things.





## Question 4 : Summary of Ted talk by Matt Walker on Sleep is your super power 

* "Sleep has a very large impact on human health. Sleep deprivation can make a person look a decade older.

* Sleep is also important in the learning process, before as well as after learning anything. Getting enough sleep before learning new things makes our brain ready to absorb more data.
According to the study conducted by the speaker, people with a good sleeping schedule are 40% more productive when learning.

* Restricting our sleep to 4 hours can lead to a 75% reduction in natural killer cells in our body, which actually helps our body stay immune to various diseases.

* Sleep deprivation degrades our DNA profile.

* Sleep-deprived people are more susceptible to getting Alzheimer's disease and dementia.

* Making a regular sleeping schedule can help us get better sleep.

* Maintaining a low body temperature and surrounding temperature can help us get sleep quickly.



## Question 5 : What are some ideas that you can implement to sleep better?


* I can go to sleep earlier if I don't use my phone right before bed.


* I can get good quality sleep if I set a regular time to go to bed.


* Going to bed early in order to get more than six hours of sleep.


* Minimising my caffeine consumption.


* Regularly practising meditation and prayer.


* Cutting back on eating before bed.



## Question 6 : Summary of Ted talk by Wendy Suzuki on The brain changeing benefits of exercise

* Speaker talks about her experience when she went on a kayaking trip by herself and found out that she was the worst person of all the people she went there with. That's when she decided that she would not be the worst person again.


* She started working on her health. and started exercising regularly; she also tried yoga, Zumba, kickboxing, and every other physical activity that could help her achieve her health goals.


* At first, it was tough for her, but later she figured out that all these physical activities boosted her mood and encouraged her to do more. and ultimately motivated her to stay focused on achieving her health goals.


* Then she shared that her efficacy in grant writing increased as she was able to focus better than before.


* According to literature, Excersing helps us get better moods, better energy,  better memory, and better attention, and she herself experienced all these changes in herself.


* Exercising can make our hippocampus and prefrontal cortex grow and our brain strong.


* The speaker says the least time we can give to exercising is 30 minutes, at least four times a week. And we don't need to go to the gym; power walking can be part of our exercise.



## Question 7 : What are some steps you can take to exercise more?

* Sparing time for exercise in your daily routine for at least 15 to 20 minutes


* Prefer walking for shorter distances instead of using any vehicle.


* learn more about exercising and identify the physical activities that I would enjoy doing.


* Try to evaluate the effects of exercising on my life, which may encourage me to do it more.


* Try to engage myself in more physical activities and do all my daily activities by myself without taking anyone's help.
