# Focus Management

## 1. What is deep work ? 

Deep work can be defined as concentrating on the work while maintaining a distraction-free environment, which can help us unlock the maximum potential of our brain. In today's world, deep work is among the rare human capabilities that have gained much importance. Study shows that people who are able to implement deep work in their daily lives are not only productive but also able to produce out-of-the-box results.


## 2. Summary of Deep Work

* Deep work was the term coined by Cal Newport, the author of "Deep Work: Rules for Focused Success in a Distracted World." In this book, he mostly talks about the conceptual foundation of deep work and strategies for how to build a deep work capability within ourselves.

* The optimal duration of the deep work goes from 1 hour to 4 hours; this time gets shorter with regular implementation of deep work. The author also talks about pulling ourselves out of the state before reaching our limit.

* As per statistics, deadlines are important for us to get artificial motivation or work pressure to deliver on time, and implementing deep work in such situations demands proper time management or scheduling our work hours.

* The author also states about the various real-life examples during the podcast where this person was able to implement deep work in their life and were able to produce the best end result, which has impacted millions of people around the globe.

* Deep work can help us get the best results that we can produce in whatever we do and impact others around us, so we should build the skill of deep work within ourselves.



## 3. Implementing Deep work principles in day to day life 

* Maintaining Fix and a regular schedule for the implementation of deep work in my daily routine

* Keeping all the distractions to a minimum and giving myself the appropriate period of my day to deal with such distractions so that I don't need to attend to them during my productive hours

* Making use of early mornings for implementing deep work as it is the best time, as suggested by the author.

* Working on small projects using deep work and gaining motivation from the end results can further increase the interest in deep work.


## 4. Key take aways from Tedtalk by Dr cal Newport on  "Quit social Media"

* We should normalize quitting social media as it is not a fundamental technology in our day-to-day lives.

* Now a days Social media is designed to keep us engaged for a longer period of time, which can take up some important hours of our day.

* Social media should be treated only as a source of entertainment and nothing more than that.

* Quitting social media can help us get better at deep work, as it will cut off 60–70% of the distractions we have in our daily lives now.

* If we are able to build the skill of deep work, then our work alone will help us get the reach that we expect from social media these days.