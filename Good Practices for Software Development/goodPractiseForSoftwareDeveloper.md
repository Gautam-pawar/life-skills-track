# Good Practise for Software Developer

## Question 1 : What is your one major takeaway from each one of the 6 sections ?

1. getting a clear understanding of the client Software development starts with requirements, so it is important to clearly document them and have the customer or the assigning authority confirm their accuracy before moving further. This will ensure that everyone is on the same page.

2. while collaborating with the team on the project While other team members may be depending on our work, it is crucial to communicate with the team members at the appropriate moment. So that they can adjust the timeframe, we should let them know if we get stopped at any point or will miss a deadline.

3. Asking for assistance when we are stuck is crucial, but how we ask for assistance is just as important. The proper technique is to describe the difficulties using various tools, such as code snippets, screenshots, screen captures, and logs, which can aid in the person's understanding of the problems.

4. While working in a team, knowing your team mates can very much help in boosting productivity as there are no communication gaps that can cause delays in information transfer within the team.

5. Being mindful while communicating with team members is important, as too much bothering can make them uncomfortable.

6. Programming is all about focus and involvement in your work. We should try to keep all the disturbances to a minimum as much as possible to get good results from our time.




## Question 2 : Which area do you think you need to improve on? What are your ideas to make progress in that area?

I think I need to work on better ways to document the project requirements and verify the documented requirements. I will make use of a notepad for writing down the requirements and get those points verified in the meeting itself.

Another thing I need to work on is being 100% involved while coding. I think keeping all devices on silent will help me avoid unnecessarily distracting activities.