# Grit & Growth Mindset

## Question 1: Summary of Tedtalk by Angela lee Duckworth 

The speaker discusses how perseverance and passion can enhance one's educational experience and aid in goal achievement. Talent alone won't guarantee success; an achiever must also have the desire to learn and develop. She comes to the conclusion that we should be more determined in everything we do or should figure out what we enjoy doing so that we can perform better.


## Question 2: Key takeaways from the video 

* Whether or not someone is talented, they can still succeed if they are passionate and persistent in their work.

* Embracing a growth mindset and being willing to make mistakes can both help us advance in life.


## Question 3: Summary of video on Growth Mindset by Trevor Ragan

Their are people with two types of mindset one is Fixed Mindset and the other one is Growth Mindset. People with fixed mindset believes that they are not in control of their abilities and so they don't try much to level up their skills.
on the other hand people with Growth Mindset believes that the skills can be built and its in their own hands, so they keep on growing as compared with the people having fixed mindset.


## Question 4 : Key takeaways from the video

* A fixed mindset will not help us advance in our careers, so we must take charge of our beliefs and adopt a growth mindset.

* A growth mindset may be a potent weapon that enables people to achieve incredible success in all aspect of modern life.


## Question 5 : Internal Locus of control

Internal locus of control is the idea that one's choices and actions may affect how things turn out in their life. Those who have an internal locus of control frequently believe that they are in charge of their life and accountable for the results of their decisions.

### Key points about Locus of control

* Having an Internal Locus of control can make an individual motivated and work hard towards his/her goals.
   
* External locus of control might cause our thinking to become more open to certain things that are influenced by outside forces in our lives.

* Teachers and mentors can work to develop students' internal locus of control.

## Question 6 : Summary of video on Growth Mindset by Brendon.com

In Developing Growth Mindset the very first step is to believe in our ability to figure things out ourselves. self-reflection and mindfulness can help us become more aware of our thoughts and emotions and to develop a positive mindset.


## Question 7 : Key points from the video

* Be proactive and take initiative. Don't wait for opportunities to come to you – seek them out and make them happen.

* A successful person's character attribute is their belief in their own skills and daily effort to push their limits.


## Question 8 : Mindset that I would like to take action on are 

1. I know more efforts lead to better understanding.

2. I will understand the users very well. I will serve them and the society by writing rock solid excellent software.


 
