## Question 1: Steps for Active Listening


1) Avoid getting distracted by your own thoughts or any other exterior factors.


2) Do not interrupt the person speaking and only speak once they are finished speaking.


3) While listening, respond positively with phrases such as "That sounds interesting," "Go ahead, I'm listening," "Tell me more," and so on.


4) Give positive feedback with your body language, like nodding to what the speaker has said.


5) Take notes on points you think are important.


6) Paraphrase what the speaker has said to make sure you are on the same page.




## Question 2: Key points of Reflective listening


### 1) Set aside our point of view and condition.

It is critical to give more weight to the person speaking while listening in order to make the speaker feel valued.



### 2) Empathy

Making an emotional connection with the speaker can help us understand him or her better, and the speaker will also feel connected or relatable.


### 3) Being Sincere

We need to be sincere with the speaker while giving feedback or responding so that they can know what we think about what they have said.


### 4) Clarify with the speaker

We need to clarify for better understanding and to avoid any misunderstanding.





## Question 3: Obstacles to Listening


Things in the environment can sometimes distract me, and I become hesitant to ask more questions when my doubts are still not cleared.




## Question 4: Measures to Improve Listening


I will be trying to keep my surroundings quiet and peaceful so that I will not get distracted easily,and will try to ask more questions when I have doubts.



## Question 5: Passive communication scenarios


I switch to passive communication when I am surrounded by a new group of people with whom I am not familiar to avoid any disagreement.




## Question 6: Aggressive communication scenarios



After a long period of passive communication, I feel the urge to express myself, sometimes aggressively.



## Question 7: Passive-aggressive communication scenarios


when I don't care much about the relationship with the person I am communicating with, like when someone tries to give me unnecessary advice or tries to redirect me from what I am currently doing.




## Question 8: Measures to make communication assertive


* I will be clear with my needs or my views on the matter.


*  I will be certain of what I feel and express at the time.


* I will not wait to express myself when in communication and will express my views in a polite way if I think it may hurt others.


* I will avoid apologising and find other ways to express any dissatisfaction.








