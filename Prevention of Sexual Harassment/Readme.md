# Prevention of Sexual Harassment

## Question 1 : What kinds of behaviour cause sexual harassment?

* Making sexually explicit remarks or jokes may offend someone.


* Stalking is a form of sexual harassment and can be upsetting to anyone.


* Posting sexually explicit content might disrupt the workplace atmosphere.


* Intruding more and more into another person's private life may irritate or disturb them.

* Standing too near, or inappropriately touching.



## Question 2 : What would you do in case you face or witness any incident or repeated incidents of such behaviour?

* Notifying HR or any other higher authority that can take appropriate and diligent action against such conduct.


* Confronting the harasser head-on and having a direct conversation with him or her might also assist to end the situation.


* We can prove such instances and take severe punishment against such a person by gathering adequate evidence of such activity.


* We can get in touch with a number of regional and national entities that assist in such problems if we don't receive the required support from workplace authorities.

* I will educate myself more about sexual harassment prevention measures.