
# Tiny habits

## Key take aways from Ted talk by BJ Fogg

* Create habits to achieve some  objective rather than its outcome.

* To form a habit, one must have motivation, ability, and a trigger.

* Have little behaviours that are meant to result in a consequence.

* Tiny habits have the potential to  infiltrate your system and double the amount of effort you expend to achieve the intended result.

* Past habits can be utilised to initiate new habits.





## Key take aways from Ted talk by BJ Fogg 

* A habit can be reduced to its most basic form by either reducing the quantity or performing the initial step.

* The solution is to reduce the habit to its most basic form. This makes it easier to embrace and stick with.

* Motivation, Ability, and Prompt are the three components of behaviour.

* The habit is triggered by three prompts.

    1. Prompt from outside (Phone notification)

    2. Internal provocation (thoughts and emotions)

    3. Prompt for action (chaining of a tiny behaviour with an existing behaviour)  


* The action prompt is the most effective method for forming a new habit. This suggestion applies the momentum we already have to a new minor task.

* The most important aspect of habit development is learning to celebrate after completing a task. It maintains incentive for repeating the behaviour.    




##  How can you use B = MAP to make it simpler to form new habits?

  1. I'll make my bed as soon as I wake up.

  2. I'm going for a short walk after dinner.

  3. I'll think of one wonderful thing that happened that day before going to bed.
  
  4. I'll do excercise in the morning for atleast 20 minutes. 


  ## Question 4 : Why it is important to "Shine" or Celebrate after each successful completion of habit?

  After fulfilling a habit, it is crucial to celebrate since it boosts confidence and motivation, creating a drive to push oneself further and expand the habit.


  ## Question 5 : Key take aways from video : 1% Better Every Day

* If I specify a time and location for conducting an activity, the likelihood of doing that activity improves.

* Many people believe they lack motivation when, in fact, they lack clarity.

* The environment shapes desire. We should shape the environment to suit our needs.

* Quantity works more better than quality in terms of an activity.

* The Seinfield Theory talks about not breaking the chain of a habit. If you’ve adopted a habit, do it every single day.


## Question 6 : Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

 Our approach to developing a habit is diametrically opposed. We aim to form a habit and then desire that after doing so for a long enough period of time. It will become our brand. Rather, we should consider identity first. For example, if I want to be healthy, I should see myself as a fit person who eats well, avoids bad food, and exercises consistently.


 ## Question 7 : Write about the book's perspective on how to make a good habit easier?

* Make It Clearly Visible.

* Make it appealing.

* Make it Simple.

* Make it Satisfying.



## Question 8 : Write about the book's perspective on making a bad habit more difficult?


* Create it invisible.

* Make it ugly.

* Make it challenging.

* Make it unsatisfactory.

## Question 9 : Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

Every night before I go to bed, I'll read a book.
Keeping the book I'm reading beside my bedside table will make it simpler, and after 20 minutes of reading, I'll reward myself with a nap.


## Question 10 : Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

* I'm going to stop using my phone right before bed.I would convince myself by reading some articles on the demerits of using phone in bed.


