# Learning Process 

## Question 1: Feynman Technique

The Feynman technique is named after Nobel prize winning physicist Richard Feynman ,
It is the unique way of learning & understanding any topic.

The technique is divides in few steps whch are as follows 

### Step 1 : Study

select any topic you wish and study  from any resource or meduim you find suitable .

### Step 2 : Teach

once you have completed studied the topic , you should teach someone who is not aware of the topic.
while teaching you will come across some gaps or portion of topic which you are not comfortable with .

### step 3 : Fill the gaps

Now you need to go back and study the gaps or portions that you were not comfartable with .

### step 4 : Simplify

Now as you have relearnt the topic now you should try to simplify the topic in such a way that small kids  would be easily able to understand .


## Question 2: Ways to implement technique in my learning process

* adhering to the steps outlined in the timeline and incorporating the technique into my learning process.

* I can use this method to master the technical skills I currently possess by teaching them to a newbie who intends to learn them.

Various modes of explanation, like graphical and verbal,  can help me make use of the technique.


## Question 3: Summary of the Tedtalk on How to learn by Barbara Oakley

The speaker, Barbara Oakley, talks about a unique learning technique that she herself experienced and learned over years. She also discusses the neuroscience behind the technique.

The technique is:
Our brain learns new information in two states: focused and diffuse. When we learn anything in focused mode, our brain follows the same neural pathways to learn and understand the topic.
When we are in a diffuse state, our brain forms or goes through new neural pathways, which leads to new ideas or perspectives about the matter, which helps us solve problems or gives us a deeper understanding of the topic we are learning. That's why the speaker says it is important to implement learning in both states for optimum results.


## Question 4: Steps for Improving my learning process

* Making use of both focused and diffused modes might help me understand topics where I am getting stuck and not able to move ahead in my learning process.

* Scheduling my learning time for focused and diffused states will help me maintain balance between both states .

* Taking detailed notes will assist me in understanding and revising the topic in the future. 


## Question 5: Summary of Tedtalk on How to learn anything  by Josh Kaufman 

* The speaker says that according to the professor at Florida state university K.Andreson, It take roughly 10,000 hrs to master any skill. as per their research on varoius elite proffesionals from various fields . The study also says that during this time we go through diffrent phases of learning and the output of time spent and topic learnt changes considerably.

The speaker further says. we dont need to spend this much time as we dont intend to master every skill and just need to be good enough that we can operate and work with this skills. 

speaker has divded this 20 hrs technique in 4 steps which are as follows.

### step 1:Deconstruct the skill 

Breakdown the skill into smaller pieces and set priorities to this pieces.

### step 2:Learn enough to self-correct

Gather three to five resources of the topic & learn just enough that we can practise and self correct ourself.

### step 3: Remove the practise barriers

Removing all the distractions like Internet,television and smartphones so that you are able to concentrate on what you intend to learn.

### step 4:Practise atleast 20 hours 

Making a comitment to practising whatever you learn for atleast 20 hours which will help you gain the rewards of time you have invested in the learning.


## Question 6 : Steps for learning new topic 

### Step 1 : Get myself introduced to the topic

### Step 2: Collect Resources in Various Mediums such as Books, Videos, Courses, Articles, and so on.

### Step 3: Make a plan to study and practise continuously.

### Step 4: Making application of various learning techniques like Fenyman, Pomodoro, and the Twenty-Hour Technique

### Step 5: validate my knowldege by assesments or reviews . 




